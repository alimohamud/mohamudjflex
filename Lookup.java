

import java.util.HashMap;

public class Lookup extends HashMap<String, TokenType> {
  public Lookup() {
    this.put(";", TokenType.SEMI_COLON);
    this.put("||", TokenType.OR);
    this.put("(", TokenType.OPEN_PARENS);
    this.put("[", TokenType.OPEN_BRACKET);
    this.put("{", TokenType.OPEN_BRACE);
    this.put(")", TokenType.CLOSE_PARENS);
    this.put("]", TokenType.CLOSE_BRACKET);
    this.put("}", TokenType.CLOSE_BRACE);
    this.put("=", TokenType.EQUAL);
    this.put("+", TokenType.ADD);
    this.put("-", TokenType.SUBTRACT);
    this.put("*", TokenType.MULTIPLY);
    this.put("/", TokenType.DIVIDE);
    this.put("*", TokenType.MULTIPLY);
    this.put("<", TokenType.LESS_THAN);
    this.put(">", TokenType.GREATER_THAN);
    this.put("<=", TokenType.LESS_EQUAL);
    this.put(">=", TokenType.GREATER_EQUAL);
    this.put("!", TokenType.NOT);
    this.put("!=", TokenType.NOT_EQUAL);
    this.put("&&", TokenType.AND);
    this.put("||", TokenType.OR);
    this.put("char", TokenType.CHARACTER);
    this.put("int", TokenType.INTEGER);
    this.put("float", TokenType.FLOATING_POINT);
    this.put("if", TokenType.IF_STATEMENT);
    this.put("else", TokenType.ELSE_CLAUSE);
    this.put("while", TokenType.WHILE_LOOP);
    this.put("print", TokenType.PRINT_STATEMENT);
    this.put("return", TokenType.RETURN);
    this.put("func", TokenType.FUNCTION);
    this.put("program", TokenType.PROGRAM);
    this.put("end", TokenType.END);
    this.put("read", TokenType.READ);

  }
}
