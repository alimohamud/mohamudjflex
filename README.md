This is the README for the Flex Project. This project contains the FlexScanner.flex file to create a java program, Scanner.java. This program will take in a .txt file and parse through returning each word, number, and known symbol within the file. This is done using Token.java, TokenTypes.java and Lookup.java. There are also two possible input files first the SampleInput.txt to see the program run as it's supposed to and then the ErrorInput.txt which generates errors for each of the unknown characters contained in the file. There is also a Main.java which serves as the driver for the project.

To run this first you need to create the Scanner.java class using the flex file with the following command once you cd into this project folder. Then yout type in "java -jar jflex.jar FlexScanner.flex" on the command line. 

Then you need to compile the main.java file by typing in the command "javac Main.java".

This step should create a Main.class file.  With this you can type in "java Main SampleInput.txt" to run the program with the text file that comes with the project. Replace SampleInput.txt with the name of any other .txt file you'd like to evaluate to run the program through that. 


