/*
 *
 * By: Ali Mohamud
 *
 */


 /* User Comments and Code */

/*
This is a lexical analyzer for a small C-like language.
*/







%%



 /*   Directives and Macros    */
 /*
  *  %class    ClassName   Name of generated class
  *  %function name        Name of the scanning method
  *  %integer              Causes the return type of scanning method to be int
  *  %type    TypeName     Return type of scanning method will be TypeName
  *  %eofval{              Code to return some value at the end of file, this
  *  %eofval}                must match the return type of scanning method.
  *
  *  %standalone          Creates a main method in generated class
  *
  *  Macros are of the form MacroName = regular expression
  *  To use an already-defined macro in a regular expression, use {MacroName}
  */

%public
%class Scanner
%function next
%type    Token
%eofval{
   System.out.println("Reached end of file.");

   return null;
%eofval}
%{
  private Lookup table = new Lookup();
%}

other 	= .

digit	= [0-9\.]

integer = {digit}+

ID = ([A-Za-z][A-Za-z0-9]*)

whitespace	= [ \n\t]

operators = [\*\+\-\/\;\(\)\[\]\{\}\<\>\=\!\&\|]

symbol = {operators}+

keyword = "int" | "char" | "float" | "if" | "else" |"while" | "print" | "read" |"return" | "func" | "program" | "end"

comment = "/*"( [^*] | (\*+[^*/]) )*\*+\/

slcomment = "//" ([a-zA-Z0-9] | [*] | [/] | [+] | [-] | [ \t\f])* (\r|\n|\r\n)


%%
/* Lexical Rules  */
/* The rules for what to do when a pattern is matched.
 * These are of the form {patternname} {code to run on match}
 */


 {keyword} {

  	//System.out.println("Found word: " + yytext());

  	return new Token( yytext(), table.get( yytext()));

  }



{integer} {

	//System.out.println("Found number: " + yytext());

	return new Token( yytext(), TokenType.NUMBER);

}

{ID} {
  return new Token( yytext(), TokenType.IDENTIFIER);
}

{symbol} {
  return new Token( yytext(), table.get( yytext()));
}

{whitespace} {
	//ignore whitespace
}

{comment} {
  System.out.println("Commented out: " + yytext());
}
{slcomment} {
  System.out.println("Commented out: " + yytext());
}
{other} {
       //ignore other stuff
       System.out.println("ERROR - Unknown character detected: " + yytext());
}
