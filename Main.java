/*
 * Main.java
 */

/**
 * Main class for testing our scanner.
 */

public class Main {

    public static void main( String[] args) {
        if (args.length == 0) {
           System.out.println("Usage : java Scanner [ --encoding <name> ] <inputfile(s)>");
        }
        else {
      int firstFilePos = 0;
      String encodingName = "UTF-8";
      for (int i = firstFilePos; i < args.length; i++) {
        Scanner scanner = null;
        try {
          java.io.FileInputStream stream = new java.io.FileInputStream(args[i]);
          java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
          scanner = new Scanner(reader);
          while ( !scanner.yyatEOF() ) {

              Token tok = scanner.next();
              System.out.println("Got a token from the scanner: " + tok );
          }
        }
        catch (java.io.FileNotFoundException e) {
          System.out.println("File not found : \""+args[i]+"\"");
        }
        catch (java.io.IOException e) {
          System.out.println("IO error scanning file \""+args[i]+"\"");
          System.out.println(e);
        }
        catch (Exception e) {
          System.out.println("Unexpected exception:");
          e.printStackTrace();
        }
      }
    }
  }


}
