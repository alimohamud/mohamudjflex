/*
 * TokenType.java
 */

/**
 * Enum of TokenTypes
 */
public enum TokenType {

    NUMBER,
    IDENTIFIER,
    CHARACTER,
    INTEGER,
    FLOATING_POINT,
    IF_STATEMENT,
    ELSE_CLAUSE,
    WHILE_LOOP,
    PRINT_STATEMENT,
    READ,
    RETURN,
    FUNCTION,
    PROGRAM,
    END,
    SEMI_COLON,
    OPEN_PARENS,
    CLOSE_PARENS,
    OPEN_BRACKET,
    CLOSE_BRACKET,
    OPEN_BRACE,
    CLOSE_BRACE,
    EQUAL,
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
    LESS_THAN,
    GREATER_THAN,
    LESS_EQUAL,
    GREATER_EQUAL,
    NOT,
    NOT_EQUAL,
    AND,
    OR,



}
